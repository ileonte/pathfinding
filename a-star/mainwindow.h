#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

#include <griddisplay.h>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    GridDisplay* display_;
};

#endif // MAINWINDOW_H
