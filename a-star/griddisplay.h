#ifndef GRIDDISPLAY_H
#define GRIDDISPLAY_H

#include <QWidget>
#include <QBrush>
#include <QRect>
#include <QVector>

#include <grid.h>

class GridDisplay : public QWidget
{
    Q_OBJECT
public:
    explicit GridDisplay(QWidget *parent = nullptr);

signals:
    void canSimulate(bool);
    void error(const QString&);

public slots:
    void setGrid(const Grid& grid);
    void clearGrid();
    void loadMap(const QString& path);
    void canMoveDiagonally(bool yesno);
    void simulationStep();
    void simulationComplete();
    void simulationReset();

private:
    void paintEvent(QPaintEvent* ev) final;
    void resizeEvent(QResizeEvent* ev) final;
    void mousePressEvent(QMouseEvent* ev) final;

    QRect rectForGridPosition(int x, int y) const;
    QRect rectForGrid() const;
    void dimensionsForGrid(QRect& gridRect, int& tileSize) const;
    void recalculateRects();

    Grid grid_{};
    AStar astar_{};

    QRect gridRect_{};
    int tileSize_{1};
    QBrush wall_brush_{};
    QBrush floor_brush_{};
    QBrush start_brush_{};
    QBrush end_brush_{};
    QBrush open_brush_{};
    QBrush closed_brush_{};
    QBrush path_brush_{};
    QVector<QRect> walls_;
    QVector<QRect> floors_;
    QRect start_{};
    QRect end_{};
};

#endif // GRIDDISPLAY_H
