#include "griddisplay.h"

#include <QPainter>
#include <QPaintDevice>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QtDebug>

GridDisplay::GridDisplay(QWidget *parent)
    : QWidget(parent)
    , wall_brush_(Qt::GlobalColor::black)
    , floor_brush_(Qt::GlobalColor::white)
    , start_brush_(Qt::GlobalColor::blue)
    , end_brush_(Qt::GlobalColor::darkBlue)
    , open_brush_(Qt::GlobalColor::green)
    , closed_brush_(Qt::GlobalColor::red)
    , path_brush_(Qt::GlobalColor::lightGray)
{
    setAttribute(Qt::WA_OpaquePaintEvent);
    setMinimumSize(1024, 900);
}

void GridDisplay::loadMap(const QString& path) {
    Grid grid{};
    auto ret = grid.load_from_file(path.toUtf8().data());
    if (ret) {
        emit error(ret.value().c_str());
        return;
    }

    setGrid(grid);
}

QRect GridDisplay::rectForGridPosition(int x, int y) const
{
    return QRect(gridRect_.left() + x * tileSize_,
                 gridRect_.top() + y * tileSize_,
                 tileSize_,
                 tileSize_);
}

QRect GridDisplay::rectForGrid() const
{
    if (grid_.is_empty()) return rect();
    int width = size().width();
    int height = size().height();
    int ss = std::min(width / grid_.width(), height / grid_.height());
    int topy = (height - ss * grid_.height()) / 2;
    int topx = (width - ss * grid_.width()) / 2;
    return QRect(topx, topy, ss * grid_.width(), ss * grid_.height());
}

void GridDisplay::dimensionsForGrid(QRect& gridRect, int& tileSize) const
{
    if (grid_.is_empty()) {
        gridRect = QRect();
        tileSize = 1;
        return;
    }

    gridRect = rectForGrid();
    tileSize = gridRect.width() / grid_.width();
}

void GridDisplay::recalculateRects()
{
    if (grid_.is_empty()) return;

    dimensionsForGrid(gridRect_, tileSize_);

    walls_.reserve(grid_.width() * grid_.height());
    floors_.reserve(grid_.width() * grid_.height());
    walls_.clear();
    floors_.clear();

    for (int dy = 0; dy < grid_.height(); dy++) {
        for (int dx = 0; dx < grid_.width(); dx++) {
            switch (grid_.at(dx, dy)) {
                case tile_type::wall: {
                    walls_.append(rectForGridPosition(dx, dy));
                    break;
                }
                case tile_type::floor: {
                    floors_.append(rectForGridPosition(dx, dy));
                    break;
                }
            }
        }
    }



    if (grid_.start_point())
        start_ = rectForGridPosition(grid_.start_point()->x, grid_.start_point()->y);
    if (grid_.end_point())
        end_ = rectForGridPosition(grid_.end_point()->x, grid_.end_point()->y);
}

void GridDisplay::setGrid(const Grid& grid)
{
    if (grid.is_empty()) return;
    clearGrid();
    grid_ = grid;
    recalculateRects();
    update();
}

void GridDisplay::clearGrid()
{
    grid_.clear();
    astar_.clear();

    walls_.clear();
    floors_.clear();
    start_ = QRect();
    end_ = QRect();

    error("");

    update();

    emit canSimulate(false);
}

void GridDisplay::canMoveDiagonally(bool yesno)
{
    astar_.can_move_diagonally(yesno);
    simulationReset();
}

void GridDisplay::paintEvent(QPaintEvent* ev)
{
    QPainter painter(this);
    painter.fillRect(rect(), Qt::GlobalColor::white);

    painter.setBrush(floor_brush_);
    painter.drawRects(floors_);

    painter.setBrush(wall_brush_);
    painter.drawRects(walls_);

    if (!astar_.is_empty()) {
        painter.setBrush(open_brush_);
        for (const auto& o : astar_.open_set()) {
            QRect r{rectForGridPosition(o.x, o.y)};
            const auto& node = astar_.at(o);
            painter.drawRect(r);
            painter.drawText(r, Qt::AlignCenter, QString("%1\n%2").arg(node.g_cost).arg(node.h_cost));
        }

        painter.setBrush(closed_brush_);
        for (const auto& c : astar_.closed_set()) {
            QRect r{rectForGridPosition(c.x, c.y)};
            const auto& node = astar_.at(c);
            painter.drawRect(r);
            painter.drawText(r, Qt::AlignCenter, QString("%1\n%2").arg(node.g_cost).arg(node.h_cost));
        }
    }

    if (astar_.is_done()) {
        painter.setBrush(path_brush_);
        for (auto p : astar_.path()) {
            QRect r{rectForGridPosition(p.x, p.y)};
            const auto& node = astar_.at(p);
            painter.drawRect(r);
            painter.drawText(r, Qt::AlignCenter, QString("%1\n%2").arg(node.g_cost).arg(node.h_cost));
        }
    }

    if (!start_.isEmpty()) {
        painter.setBrush(start_brush_);
        painter.drawRect(start_);
    }
    if (!end_.isEmpty()) {
        painter.setBrush(end_brush_);
        painter.drawRect(end_);
    }

    ev->accept();
}

void GridDisplay::resizeEvent(QResizeEvent* ev)
{
    recalculateRects();
    ev->accept();
}

void GridDisplay::mousePressEvent(QMouseEvent* ev)
{
    if (grid_.is_empty()) return;

    QPoint localPoint = ev->localPos().toPoint();
    bool do_update{false};

    if (gridRect_.contains(localPoint)) {
        localPoint -= gridRect_.topLeft();
        int dx = localPoint.x() / tileSize_;
        int dy = localPoint.y() / tileSize_;
        QRect rr = rectForGridPosition(dx, dy);

        if (!grid_.is_blocked(dx, dy)) {
            if (ev->button() == Qt::MouseButton::LeftButton && rr != end_) {
                grid_.set_start_point(dx, dy);
                start_ = rr;
                simulationReset();
            }
            if (ev->button() == Qt::MouseButton::RightButton && rr != start_) {
                grid_.set_end_point(dx, dy);
                end_ = rr;
                simulationReset();
            }
        }
    }

    ev->accept();

    if (do_update) update();

    emit canSimulate(grid_.start_point().has_value() && grid_.end_point().has_value());
}

void GridDisplay::simulationStep()
{
    if (astar_.is_empty()) {
        if (!astar_.init(grid_)) {
            emit error("Failed to initialize simulation");
            return;
        }
    }

    astar_.step();
    if (astar_.is_done()) {
        if (astar_.is_reachable())
            error(QString("Destination reachable in %1 steps").arg(astar_.path().size()));
        else
            error("Destination unreachable!");
    }

    update();
}

void GridDisplay::simulationComplete()
{
    if (astar_.is_empty()) {
        if (!astar_.init(grid_)) {
            emit error("Failed to initialize simulation");
            return;
        }
    }

    auto duration = time_call([&]() {
        while (!astar_.is_done()) {
            astar_.step();
        }
    });
    if (astar_.is_reachable())
        error(QString("Destination reachable in %1 steps (%2 ns)").arg(astar_.path().size()).arg(duration.count()));
    else
        error(QString("Destination unreachable! (%1 ns)").arg(duration.count()));

    update();
}

void GridDisplay::simulationReset()
{
    if (grid_.start_point().has_value() && grid_.end_point().has_value()) {
        astar_.reset(grid_.start_point().value(), grid_.end_point().value());
    }
    update();
}
