#include "mainwindow.h"

#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFrame>
#include <QCheckBox>
#include <QLabel>

#include <griddisplay.h>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    auto* vl = new QVBoxLayout();
    setLayout(vl);

    display_ = new GridDisplay(this);
    vl->addWidget(display_);
    vl->setStretchFactor(display_, 99);

    {
        auto* hl = new QHBoxLayout();

        auto* el = new QLineEdit();
        el->setReadOnly(true);
        connect(display_, &GridDisplay::error, el, &QLineEdit::setText);
        hl->addWidget(el);
        hl->setStretchFactor(el, 99);

        auto* bb = new QPushButton("&Open");
        connect(bb, &QPushButton::pressed, [this]() {
            auto path = QFileDialog::getOpenFileName(this, "Select board", ".", "Boards (*.txt)");
            if (path.isEmpty()) return;
            display_->loadMap(path);
        });
        hl->addWidget(bb);

        bb = new QPushButton("&Clear");
        connect(bb, &QPushButton::pressed, [this, el]() {
            display_->clearGrid();
            el->clear();
        });
        hl->addWidget(bb);

        auto* ff = new QFrame();
        ff->setFrameStyle(QFrame::VLine);
        hl->addWidget(ff);

        hl->addWidget(new QLabel("Simulation:"));

        auto* kb = new QCheckBox("Allo&w diagonal movement");
        kb->setDisabled(true);
        connect(display_, &GridDisplay::canSimulate, kb, &QCheckBox::setEnabled);
        connect(kb, &QCheckBox::toggled, display_, &GridDisplay::canMoveDiagonally);
        hl->addWidget(kb);

        bb = new QPushButton("&Step");
        bb->setDisabled(true);
        connect(display_, &GridDisplay::canSimulate, bb, &QPushButton::setEnabled);
        connect(bb, &QPushButton::pressed, display_, &GridDisplay::simulationStep);
        hl->addWidget(bb);

        bb = new QPushButton("C&omplete");
        bb->setDisabled(true);
        connect(display_, &GridDisplay::canSimulate, bb, &QPushButton::setEnabled);
        connect(bb, &QPushButton::pressed, display_, &GridDisplay::simulationComplete);
        hl->addWidget(bb);

        bb = new QPushButton("&Reset");
        bb->setDisabled(true);
        connect(display_, &GridDisplay::canSimulate, bb, &QPushButton::setEnabled);
        connect(bb, &QPushButton::pressed, display_, &GridDisplay::simulationReset);
        hl->addWidget(bb);

        vl->addLayout(hl);
        vl->setStretchFactor(hl, 1);
    }

    display_->loadMap("/home/ileonte/Work/projects/A-star/vid1.txt");
}

MainWindow::~MainWindow()
{
}
